// testshapes demo for Adafruit RGBmatrixPanel library.
// Demonstrates the drawing abilities of the RGBmatrixPanel library.
// For 32x32 RGB LED matrix:
// http://www.adafruit.com/products/607

// Written by Limor Fried/Ladyada & Phil Burgess/PaintYourDragon
// for Adafruit Industries.
// BSD license, all text above must be included in any redistribution.

#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// If your 32x32 matrix has the SINGLE HEADER input,
// use this pinout:
#define CLK 8  // MUST be on PORTB! (Use pin 11 on Mega)
#define OE  9
#define LAT 10
#define A   A0
#define B   A1
#define C   A2
#define D   A3

RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false);

const int pirPin=12;

const int photoResist=4;

const int FREE = 1;
const int UNKNOW = 2;
const int UNCLEAR = 3;
const int DEATH = 4;

const int DARK = 70;

long LIFE_TIME = 50000;
long PERIOD_TIME = 20000;

const int EMPTY = 0;
const int FULL = 1;

const int MAX_COLOR = 100;

int currentStatus = 0;
int lastStatus = 0;
int inside = EMPTY;

long session_time = 0;//общее время
long current_period_time = 0;//счетчик для итерации
long time_left = 0;



void setup() {
  Serial.begin(9600);
  matrix.begin();
}


void loop() {

    // если включен свет
    if ( analogRead(photoResist) > DARK) {
   
    //внутри сидит ктото
    if (inside == FULL) {
      time_left = millis();
      delay(10);
      session_time = session_time + watch(time_left); //получаем новое значение времени
      current_period_time = current_period_time + watch(time_left); //получаем новое значение времени

      //внутри пусто
      } else if (inside == EMPTY) {
        currentStatus = UNKNOW;
        time_left = 0;
        session_time = 0;
        current_period_time = 0;
      }

    //засек движение
    if (moveOn()) {
        inside = FULL;
        current_period_time = 0; //сбрасываем отсчет времени итерации

        //Ставим статусы занятости
        if (session_time >= LIFE_TIME) {
            currentStatus = DEATH;

        } else {
            currentStatus = UNCLEAR;
        } 


    //не засек движения
    } else {
    //итерация окончилась
    if (current_period_time >= PERIOD_TIME) {
      inside = EMPTY;
    //по хорошему тут бы закрыть, но мы должны узнать не стоит ли череп 
    //итерация продолжается
    } else {
      if (currentStatus != DEATH && currentStatus != UNKNOW) {
        currentStatus = UNCLEAR;
      } 
    }
    }
    
    //свет выключен
    } else {
        currentStatus = FREE;
        session_time = 0;
    current_period_time = 0;
        inside = EMPTY;
    }


//обработка статусов     
    if ( currentStatus != lastStatus ) {
    matrix.fillScreen(matrix.Color333(0, 0, 0));
  }

  if (currentStatus == DEATH && lastStatus != DEATH) {
    lastStatus = DEATH;
    skull(MAX_COLOR,MAX_COLOR,MAX_COLOR);
  }

  if (currentStatus == UNCLEAR && lastStatus != UNCLEAR) {
    lastStatus = UNCLEAR;
    shit(MAX_COLOR,0,0);
  } 
  
  if (currentStatus == FREE && lastStatus != FREE) {
    lastStatus = FREE;
    inside = EMPTY; 
    smile(0, MAX_COLOR, 0);
  }
  
  if (currentStatus == UNKNOW && lastStatus != UNKNOW) {
    lastStatus = UNKNOW;
    session_time = 0;
    question(MAX_COLOR, MAX_COLOR, 0);
  }

}


bool moveOn() {
  //Если обнаружено движение
  if(digitalRead(pirPin) == HIGH) {
    return 1;
  }        

  //Ели движения нет
  if(digitalRead(pirPin) == LOW) {      
    return 0;
  }
}


 
 int watch(int watch_t) {
  unsigned long time_;
  long watch_delta;
  time_ = millis();
  watch_delta = time_ - watch_t; 
  return watch_delta ;
}

void skull(int r, int g, int b) { 
  //Короче смотри, X0, Y0, X1, Y1, COLOR
  //X0 - номер столба начала отрисовки по X
  //Y0 - номер строки начала отрисовки по y
  //X1 - номер столба конца отрисовки по x
  //Y1 - номер строки конца отрисовки по y
 
  matrix.drawLine(14, 4, 17, 4, matrix.Color333(r, g, b));
  matrix.drawLine(11, 5, 20, 5, matrix.Color333(r, g, b));
  matrix.drawLine(9, 6, 22, 6, matrix.Color333(r, g, b));
  matrix.drawLine(8, 7, 23, 7, matrix.Color333(r, g, b));
  matrix.drawLine(7, 8, 24, 8, matrix.Color333(r, g, b));
  matrix.drawLine(6, 9, 25, 9, matrix.Color333(r, g, b));
  matrix.drawLine(6, 10, 25, 10, matrix.Color333(r, g, b));
  matrix.drawLine(5, 11, 26, 11, matrix.Color333(r, g, b));
  matrix.drawLine(5, 12, 26, 12, matrix.Color333(r, g, b));
  //Конец лба
  //Начала глаз
  matrix.drawLine(4, 13, 7, 13, matrix.Color333(r, g, b));
  matrix.drawLine(10, 13, 12, 13, matrix.Color333(r, g, b));
  matrix.drawLine(15, 13, 16, 13, matrix.Color333(r, g, b));
  matrix.drawLine(19, 13, 21, 13, matrix.Color333(r, g, b));
  matrix.drawLine(24, 13, 27, 13, matrix.Color333(r, g, b));

  matrix.drawLine(4, 14, 8, 14, matrix.Color333(r, g, b));
  matrix.drawLine(11, 14, 11, 14, matrix.Color333(r, g, b));
  matrix.drawLine(14, 14, 17, 14, matrix.Color333(r, g, b));
  matrix.drawLine(20, 14, 20, 14, matrix.Color333(r, g, b));
  matrix.drawLine(23, 14, 27, 14, matrix.Color333(r, g, b));

  matrix.drawLine(4, 15, 9, 15, matrix.Color333(r, g, b));
  matrix.drawLine(13, 15, 18, 15, matrix.Color333(r, g, b));
  matrix.drawLine(22, 15, 27, 15, matrix.Color333(r, g, b));


  matrix.drawLine(4, 16, 9, 16, matrix.Color333(r, g, b));
  matrix.drawLine(13, 16, 18, 16, matrix.Color333(r, g, b));
  matrix.drawLine(22, 16, 27, 16, matrix.Color333(r, g, b));


  matrix.drawLine(4, 17, 8, 17, matrix.Color333(r, g, b));
  matrix.drawLine(11, 17, 11, 17, matrix.Color333(r, g, b));
  matrix.drawLine(14, 17, 17, 17, matrix.Color333(r, g, b));
  matrix.drawLine(20, 17, 20, 17, matrix.Color333(r, g, b));
  matrix.drawLine(23, 17, 27, 17, matrix.Color333(r, g, b));

  matrix.drawLine(4, 18, 7, 18, matrix.Color333(r, g, b));
  matrix.drawLine(10, 18, 12, 18, matrix.Color333(r, g, b));
  matrix.drawLine(15, 18, 16, 18, matrix.Color333(r, g, b));
  matrix.drawLine(19, 18, 21, 18, matrix.Color333(r, g, b));
  matrix.drawLine(24, 18, 27, 18, matrix.Color333(r, g, b));
  //Конец глаз
  //Нос
  matrix.drawLine(4, 19, 27, 19, matrix.Color333(r, g, b));
  matrix.drawLine(4, 20, 27, 20, matrix.Color333(r, g, b));

  matrix.drawLine(5, 21, 14, 21, matrix.Color333(r, g, b));
  matrix.drawLine(17, 21, 26, 21, matrix.Color333(r, g, b));

  matrix.drawLine(8, 22, 13, 22, matrix.Color333(r, g, b));
  matrix.drawLine(18, 22, 23, 22, matrix.Color333(r, g, b));

  matrix.drawLine(8, 23, 13, 23, matrix.Color333(r, g, b));
  matrix.drawLine(15, 23, 16, 23, matrix.Color333(r, g, b));
  matrix.drawLine(18, 23, 23, 23, matrix.Color333(r, g, b));

  matrix.drawLine(8, 24, 23, 24, matrix.Color333(r, g, b));
  matrix.drawLine(8, 25, 23, 25, matrix.Color333(r, g, b));

  //Конец носа
  //Зубы
  matrix.drawLine(8, 26, 11, 26, matrix.Color333(r, g, b));
  matrix.drawLine(13, 26, 15, 26, matrix.Color333(r, g, b));
  matrix.drawLine(17, 26, 19, 26, matrix.Color333(r, g, b));
  matrix.drawLine(21, 26, 23, 26, matrix.Color333(r, g, b));

  matrix.drawLine(9, 27, 10, 27, matrix.Color333(r, g, b));
  matrix.drawLine(13, 27, 14, 27, matrix.Color333(r, g, b));
  matrix.drawLine(17, 27, 18, 27, matrix.Color333(r, g, b));
  matrix.drawLine(21, 27, 22, 27, matrix.Color333(r, g, b));
}


void shit (int r, int g, int b) {

  matrix.drawLine(9, 1, 9, 1, matrix.Color333(r, g, b));
  matrix.drawLine(10, 2, 10, 2, matrix.Color333(r, g, b));
  matrix.drawLine(10, 3, 10, 3, matrix.Color333(r, g, b));
  matrix.drawLine(24, 3, 24, 3, matrix.Color333(r, g, b));

  matrix.drawLine(3, 4, 3, 4, matrix.Color333(r, g, b));
  matrix.drawLine(9, 4, 9, 4, matrix.Color333(r, g, b));
  matrix.drawLine(18, 4, 18, 4, matrix.Color333(r, g, b));
  matrix.drawLine(25, 4, 25, 4, matrix.Color333(r, g, b));

  matrix.drawLine(4, 5, 4, 5, matrix.Color333(r, g, b));
  matrix.drawLine(9, 5, 9, 5, matrix.Color333(r, g, b));
  matrix.drawLine(17, 5, 18, 5, matrix.Color333(r, g, b));
  matrix.drawLine(26, 5, 26, 5, matrix.Color333(r, g, b));

  matrix.drawLine(5, 6, 5, 6, matrix.Color333(r, g, b));
  matrix.drawLine(10, 6, 10, 6, matrix.Color333(r, g, b));
  matrix.drawLine(16, 6, 18, 6, matrix.Color333(r, g, b));
  matrix.drawLine(26, 6, 26, 6, matrix.Color333(r, g, b));

  matrix.drawLine(5, 7, 5, 7, matrix.Color333(r, g, b));
  matrix.drawLine(15, 7, 18, 7, matrix.Color333(r, g, b));
  matrix.drawLine(25, 7, 25, 7, matrix.Color333(r, g, b));

  matrix.drawLine(4, 8, 4, 8, matrix.Color333(r, g, b));
  matrix.drawLine(13, 8, 18, 8, matrix.Color333(r, g, b));
  matrix.drawLine(24, 8, 24, 8, matrix.Color333(r, g, b));

  matrix.drawLine(3, 9, 3, 9, matrix.Color333(r, g, b));
  matrix.drawLine(11, 9, 18, 9, matrix.Color333(r, g, b));
  matrix.drawLine(24, 9, 24, 9, matrix.Color333(r, g, b));

  matrix.drawLine(3, 10, 3, 10, matrix.Color333(r, g, b));
  matrix.drawLine(10, 10, 20, 10, matrix.Color333(r, g, b));
  matrix.drawLine(25, 10, 25, 10, matrix.Color333(r, g, b));

  matrix.drawLine(4, 11, 4, 11, matrix.Color333(r, g, b));
  matrix.drawLine(9, 11, 21, 11, matrix.Color333(r, g, b));
  matrix.drawLine(26, 11, 26, 11, matrix.Color333(r, g, b));

  matrix.drawLine(5, 12, 5, 12, matrix.Color333(r, g, b));
  matrix.drawLine(10, 12, 21, 12, matrix.Color333(r, g, b));

  matrix.drawLine(8, 13, 9, 13, matrix.Color333(r, g, b));
  matrix.drawLine(11, 13, 20, 13, matrix.Color333(r, g, b));
  matrix.drawLine(22, 13, 23, 13, matrix.Color333(r, g, b));

  matrix.drawLine(7, 14, 10, 14, matrix.Color333(r, g, b));
  matrix.drawLine(12, 14, 19, 14, matrix.Color333(r, g, b));
  matrix.drawLine(21, 14, 24, 14, matrix.Color333(r, g, b));

  matrix.drawLine(6, 15, 25, 15, matrix.Color333(r, g, b));

  matrix.drawLine(5, 16, 26, 16, matrix.Color333(r, g, b));
  matrix.drawLine(5, 17, 26, 17, matrix.Color333(r, g, b));
  matrix.drawLine(5, 18, 26, 18, matrix.Color333(r, g, b));
  matrix.drawLine(6, 19, 26, 19, matrix.Color333(r, g, b));
  //кончился лоб

  //глаза
  matrix.drawLine(4, 20, 5, 20, matrix.Color333(r, g, b));
  matrix.drawLine(7, 20, 9, 20, matrix.Color333(r, g, b));
  matrix.drawLine(12, 20, 19, 20, matrix.Color333(r, g, b));
  matrix.drawLine(22, 20, 24, 20, matrix.Color333(r, g, b));
  matrix.drawLine(26, 20, 27, 20, matrix.Color333(r, g, b));

  matrix.drawLine(3, 21, 8, 21, matrix.Color333(r, g, b));
  matrix.drawLine(13, 21, 18, 21, matrix.Color333(r, g, b));
  matrix.drawLine(23, 21, 28, 21, matrix.Color333(r, g, b));

  matrix.drawLine(3, 22, 8, 22, matrix.Color333(r, g, b));
  matrix.drawLine(13, 22, 18, 22, matrix.Color333(r, g, b));
  matrix.drawLine(23, 22, 28, 22, matrix.Color333(r, g, b));

  matrix.drawLine(2, 23, 9, 23, matrix.Color333(r, g, b));
  matrix.drawLine(12, 23, 18, 23, matrix.Color333(r, g, b));
  matrix.drawLine(22, 23, 29, 23, matrix.Color333(r, g, b));

  matrix.drawLine(2, 24, 29, 24, matrix.Color333(r, g, b));
  matrix.drawLine(2, 25, 29, 25, matrix.Color333(r, g, b));
  //кончились глаза

  //зубы

  matrix.drawLine(2, 26, 12, 26, matrix.Color333(r, g, b));
  matrix.drawLine(14, 26, 14, 26, matrix.Color333(r, g, b));
  matrix.drawLine(16, 26, 16, 26, matrix.Color333(r, g, b));
  matrix.drawLine(18, 26, 18, 26, matrix.Color333(r, g, b));
  matrix.drawLine(20, 26, 29, 26, matrix.Color333(r, g, b));

  matrix.drawLine(3, 27, 11, 27, matrix.Color333(r, g, b));
  matrix.drawLine(20, 27, 28, 27, matrix.Color333(r, g, b));

  matrix.drawLine(3, 28, 11, 28, matrix.Color333(r, g, b));
  matrix.drawLine(13, 28, 13, 28, matrix.Color333(r, g, b));
  matrix.drawLine(15, 28, 15, 28, matrix.Color333(r, g, b));
  matrix.drawLine(17, 28, 17, 28, matrix.Color333(r, g, b));
  matrix.drawLine(19, 28, 28, 28, matrix.Color333(r, g, b));

  matrix.drawLine(4, 29, 28, 29, matrix.Color333(r, g, b));
  matrix.drawLine(6, 30, 25, 30, matrix.Color333(r, g, b));
}

void smile(int r, int g, int b) {
  //лоб
  matrix.drawLine(12, 3, 19, 3, matrix.Color333(r, g, b));
  matrix.drawLine(10, 4, 21, 4, matrix.Color333(r, g, b));
  matrix.drawLine(8, 5, 23, 5, matrix.Color333(r, g, b));
  matrix.drawLine(7, 6, 24, 6, matrix.Color333(r, g, b));
  matrix.drawLine(6, 7, 25, 7, matrix.Color333(r, g, b));
  matrix.drawLine(5, 8, 26, 8, matrix.Color333(r, g, b));
  matrix.drawLine(5, 9, 26, 9, matrix.Color333(r, g, b));
  //глаза
  matrix.drawLine(4, 10, 9, 10, matrix.Color333(r, g, b));
  matrix.drawLine(12, 10, 19, 10, matrix.Color333(r, g, b));
  matrix.drawLine(22, 10, 27, 10, matrix.Color333(r, g, b));

  matrix.drawLine(4, 11, 8, 11, matrix.Color333(r, g, b));
  matrix.drawLine(13, 11, 18, 11, matrix.Color333(r, g, b));
  matrix.drawLine(23, 11, 27, 11, matrix.Color333(r, g, b));

  matrix.drawLine(3, 12, 8, 12, matrix.Color333(r, g, b));
  matrix.drawLine(13, 12, 18, 12, matrix.Color333(r, g, b));
  matrix.drawLine(23, 12, 28, 12, matrix.Color333(r, g, b));

  matrix.drawLine(3, 13, 9, 13, matrix.Color333(r, g, b));
  matrix.drawLine(12, 13, 19, 13, matrix.Color333(r, g, b));
  matrix.drawLine(22, 13, 28, 13, matrix.Color333(r, g, b));

  matrix.drawLine(3, 14, 28, 14, matrix.Color333(r, g, b));
  //нос
  matrix.drawLine(3, 15, 28, 15, matrix.Color333(r, g, b));
  matrix.drawLine(3, 16, 28, 16, matrix.Color333(r, g, b));
  matrix.drawLine(3, 17, 28, 17, matrix.Color333(r, g, b));
  matrix.drawLine(3, 18, 28, 18, matrix.Color333(r, g, b));
  //губы
  matrix.drawLine(3, 19, 7, 19, matrix.Color333(r, g, b));
  matrix.drawLine(10, 19, 21, 19, matrix.Color333(r, g, b));
  matrix.drawLine(24, 19, 28, 19, matrix.Color333(r, g, b));

  matrix.drawLine(4, 20, 8, 20, matrix.Color333(r, g, b));
  matrix.drawLine(11, 20, 20, 20, matrix.Color333(r, g, b));
  matrix.drawLine(23, 20, 27, 20, matrix.Color333(r, g, b));

  matrix.drawLine(4, 21, 9, 21, matrix.Color333(r, g, b));
  matrix.drawLine(13, 21, 18, 21, matrix.Color333(r, g, b));
  matrix.drawLine(22, 21, 27, 21, matrix.Color333(r, g, b));

  matrix.drawLine(5, 22, 10, 22, matrix.Color333(r, g, b));
  matrix.drawLine(21, 22, 26, 22, matrix.Color333(r, g, b));

  matrix.drawLine(5, 23, 12, 23, matrix.Color333(r, g, b));
  matrix.drawLine(19, 23, 26, 23, matrix.Color333(r, g, b));

  //ПОДБОРОДОК
  matrix.drawLine(6, 24, 25, 24, matrix.Color333(r, g, b));
  matrix.drawLine(7, 25, 24, 25, matrix.Color333(r, g, b));
  matrix.drawLine(8, 26, 23, 26, matrix.Color333(r, g, b));
  matrix.drawLine(10, 27, 21, 27, matrix.Color333(r, g, b));
  matrix.drawLine(12, 28, 19, 28, matrix.Color333(r, g, b));
    
}
 
void question (int r, int g, int b) {
  matrix.drawLine(5, 3, 26, 3, matrix.Color333(r, g, b));
  matrix.drawLine(4, 4, 27, 4, matrix.Color333(r, g, b));
  matrix.drawLine(4, 5, 27, 5, matrix.Color333(r, g, b));
  matrix.drawLine(4, 6, 27, 6, matrix.Color333(r, g, b));
  matrix.drawLine(4, 7, 27, 7, matrix.Color333(r, g, b));

  matrix.drawLine(4, 8, 13, 8, matrix.Color333(r, g, b));
  matrix.drawLine(18, 8, 27, 8, matrix.Color333(r, g, b));

  matrix.drawLine(4, 9, 11, 9, matrix.Color333(r, g, b));
  matrix.drawLine(20, 9, 27, 9, matrix.Color333(r, g, b));

  matrix.drawLine(4, 10, 11, 10, matrix.Color333(r, g, b));
  matrix.drawLine(15, 10, 16, 10, matrix.Color333(r, g, b));
  matrix.drawLine(20, 10, 27, 10, matrix.Color333(r, g, b));

  matrix.drawLine(4, 11, 11, 11, matrix.Color333(r, g, b));
  matrix.drawLine(14, 11, 17, 11, matrix.Color333(r, g, b));
  matrix.drawLine(20, 11, 27, 11, matrix.Color333(r, g, b));

  matrix.drawLine(4, 12, 17, 12, matrix.Color333(r, g, b));
  matrix.drawLine(20, 12, 27, 12, matrix.Color333(r, g, b));

  matrix.drawLine(4, 13, 16, 13, matrix.Color333(r, g, b));
  matrix.drawLine(20, 13, 27, 13, matrix.Color333(r, g, b));

  matrix.drawLine(4, 14, 15, 14, matrix.Color333(r, g, b));
  matrix.drawLine(19, 14, 27, 14, matrix.Color333(r, g, b));

  matrix.drawLine(4, 15, 14, 15, matrix.Color333(r, g, b));
  matrix.drawLine(18, 15, 27, 15, matrix.Color333(r, g, b));

  matrix.drawLine(4, 16, 14, 16, matrix.Color333(r, g, b));
  matrix.drawLine(17, 16, 27, 16, matrix.Color333(r, g, b));

  matrix.drawLine(4, 17, 14, 17, matrix.Color333(r, g, b));
  matrix.drawLine(17, 17, 27, 17, matrix.Color333(r, g, b));

  matrix.drawLine(4, 18, 27, 18, matrix.Color333(r, g, b));

  matrix.drawLine(4, 19, 14, 19, matrix.Color333(r, g, b));
  matrix.drawLine(17, 19, 27, 19, matrix.Color333(r, g, b));

  matrix.drawLine(4, 20, 13, 20, matrix.Color333(r, g, b));
  matrix.drawLine(18, 20, 27, 20, matrix.Color333(r, g, b));

  matrix.drawLine(4, 21, 13, 21, matrix.Color333(r, g, b));
  matrix.drawLine(18, 21, 27, 21, matrix.Color333(r, g, b));

  matrix.drawLine(5, 22, 14, 22, matrix.Color333(r, g, b));
  matrix.drawLine(17, 22, 26, 22, matrix.Color333(r, g, b));

  matrix.drawLine(6, 23, 25, 23, matrix.Color333(r, g, b));
  matrix.drawLine(7, 24, 24, 24, matrix.Color333(r, g, b));
  matrix.drawLine(9, 25, 22, 25, matrix.Color333(r, g, b));
  matrix.drawLine(11, 26, 20, 26, matrix.Color333(r, g, b));
  matrix.drawLine(13, 27, 18, 27, matrix.Color333(r, g, b));
  matrix.drawLine(15, 28, 16, 28, matrix.Color333(r, g, b));
}